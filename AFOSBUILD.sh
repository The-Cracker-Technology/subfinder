NRM="\033[0m"
BLD="\033[1m"
BLK="\033[1;90m"
RED="\033[1;91m"
GRN="\033[1;92m"
YEL="\033[1;93m"
BLU="\033[1;94m"
PUR="\033[1;95m"
CYN="\033[1;96m"
WHT="\033[1;97m"

cd v2/cmd/subfinder

echo -e "${YEL}Running${WHT}:${NRM} ${GRN}go build${NRM}"

go build

if [ $? -eq 0 ];
then
    echo -e "go build ${WHT}[${NRM} ${GRN}OK${NRM} ${WHT}]${NRM}\n"
else
    echo -e "\n${WHT}[${NRM} ${RED}FATAL${NRM} ${WHT}]${NRM} BUILD ERROR!!!"
    exit 1
fi

strip subfinder

cp -Rf subfinder /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
